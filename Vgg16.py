import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import os
from glob import glob
import matplotlib.pyplot as plt
import os
from tensorflow.keras.layers import Input, Lambda, Dense, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input

from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.metrics import confusion_matrix

from glob import glob
train_path = 'C:/Users/user/Downloads/plantdataset/PlantVillage_train/tomato'
valid_path = 'C:/Users/user/Downloads/plantdataset/PlantVillage_valid/tomato'
image_files = glob(train_path + '/*/*.jp*g')
valid_image_files = glob(valid_path + '/*/*.jp*g')
#print(image_files)
# Get number of classes
folders = glob(train_path + '/*')

# Resize all the images to this
IMAGE_SIZE = [100, 100]
# Training config
epochs = 5
batch_size = 32
vgg = VGG16(input_shape=IMAGE_SIZE + [3], weights='imagenet', include_top=False)
# Don't train existing weights
for layer in vgg.layers:
  layer.trainable = False
#'../input/keras-pretrained-models/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'
x = Flatten()(vgg.output)
prediction = Dense(len(folders), activation='softmax')(x)

#-------------------------------
# Create Model
model = Model(inputs=vgg.input, outputs=prediction)

# View structure of the model
print(model.summary())

# Configure model
model.compile(
  loss='categorical_crossentropy',
  optimizer='rmsprop',
  metrics=['accuracy']
)
#-----------------------------------
# Create an instance of ImageDataGenerator
gen = ImageDataGenerator(
  rotation_range=20,
  width_shift_range=0.1,
  height_shift_range=0.1,
  shear_range=0.1,
  zoom_range=0.2,
  horizontal_flip=True,
  vertical_flip=True,
  rescale=1./255,
  preprocessing_function=preprocess_input
)

# Get label mapping of class and label number
test_gen = gen.flow_from_directory(valid_path, target_size=IMAGE_SIZE)
print(test_gen.class_indices)
labels = [None] * len(test_gen.class_indices)
for k, v in test_gen.class_indices.items():
  labels[v] = k

#-------------------------------------------------------------
# Create generators for training and validation
train_generator = gen.flow_from_directory(
  train_path,
  target_size=IMAGE_SIZE,
  shuffle=True,
  batch_size=batch_size,
)
valid_generator = gen.flow_from_directory(
  valid_path,
  target_size=IMAGE_SIZE,
  shuffle=False,
  batch_size=batch_size,
)
# Fit the model
r = model.fit_generator(
  train_generator,
  validation_data=valid_generator,
  epochs=epochs,
  steps_per_epoch=len(image_files) // batch_size,
  validation_steps=len(valid_image_files) // batch_size,
)
vgg_model_accuracy = model.evaluate_generator(valid_generator)[1]
print("Accuracy of VGG16 model on validation data: ", vgg_model_accuracy)



